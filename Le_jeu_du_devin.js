var nbrEssai = 0;
var result;


function retry() {
    result = Math.floor(Math.random() * 100);
    console.log(result);
    nbrEssai = 0;
}

retry();

function devin(num) {

    var n = num[0].value;
    nbrEssai++;

    if (n != null){

        var txt=window.document.getElementById('MSG');

        if (n == result) {
            txt.innerHTML = "Bravo, vous avez trouvé le nombre avec " + nbrEssai + " essais !";
        } else if (n < result){
            txt.innerHTML = "Raté, le nombre est plus élevé !";
        } else {
            txt.innerHTML = "Raté, le nombre est plus faible !";
        }
    }

}
